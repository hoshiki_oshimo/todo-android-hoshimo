package com.example.todo_app

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.graphics.Color
import android.icu.text.SimpleDateFormat
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_todo_edit.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class TodoEditFragment : Fragment() {
    private val args by navArgs<TodoEditFragmentArgs>()
    private val format = SimpleDateFormat(FORMAT_DATE, Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_todo_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button_registration.isEnabled = false
        text_title_max_limit.text = getString(R.string.text_max_limit, MAX_TITLE_LIMIT)
        text_detail_max_limit.text = getString(R.string.text_max_limit, MAX_DETAIL_LIMIT)
        setUpEditTextChangedListener()
        text_date_content.setOnClickListener {
            showDatePickerDialog()
        }
        val todo = args.todo
        if (todo != null) {
            setUpTexts(todo)
        }
        setUpButton(todo?.id)
    }

    private fun setUpEditTextChangedListener() {
        edit_text_title.addTextChangedListener {
            changeTextTitleWordCounterColor()
            changeRegistrationButtonState()
            text_title_word_counter.text = edit_text_title.length().toString()
        }

        edit_text_detail.addTextChangedListener {
            changeTextDetailWordCounterColor()
            changeRegistrationButtonState()
            text_detail_word_counter.text = edit_text_detail.length().toString()
        }
    }

    private fun changeTextTitleWordCounterColor() {
        if (edit_text_title.length() <= MAX_TITLE_LIMIT) {
            text_title_word_counter.setTextColor(Color.GRAY)
        } else {
            text_title_word_counter.setTextColor(Color.RED)
        }
    }

    private fun changeTextDetailWordCounterColor() {
        if (edit_text_detail.length() <= MAX_DETAIL_LIMIT) {
            text_detail_word_counter.setTextColor(Color.GRAY)
        } else {
            text_detail_word_counter.setTextColor(Color.RED)
        }
    }

    private fun changeRegistrationButtonState() {
        val titleTextValid = edit_text_title.length() in 1..MAX_TITLE_LIMIT
        val detailTextValid = edit_text_detail.length() <= MAX_DETAIL_LIMIT
        button_registration.isEnabled = titleTextValid && detailTextValid
    }

    private fun showDatePickerDialog() {
        val date = Calendar.getInstance()
        val datePickerDialog = DatePickerDialog(
            requireContext(),
            DatePickerDialog.OnDateSetListener { _, year, month, day ->
                text_date_content.text = ("$year/${month + 1}/$day")
            },
            date.get(Calendar.YEAR),
            date.get(Calendar.MONTH),
            date.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.setButton(
            DialogInterface.BUTTON_NEUTRAL,
            getString(R.string.button_delete)
        ) { _, _ ->
            text_date_content.text = ""
        }
        datePickerDialog.show()
    }

    private fun setUpTexts(todo: Todo) {
        edit_text_title.setText(todo.title)
        todo.detail?.let {
            edit_text_detail.setText(todo.detail)
        }
        todo.date?.let {
            text_date_content.text = format.format(todo.date)
        }
    }

    private fun setUpButton(id: Int?) {
        button_registration.text =
            getString(if (id == null) R.string.button_registration else R.string.button_update)

        button_registration.setOnClickListener {
            CoroutineScope(Default).launch {
                sendTodo(id)
            }
        }
    }

    private suspend fun sendTodo(id: Int?) {
        try {
            val request = ApiClient().apiRequest
            val response =
                if (id == null) {
                    request.createTodo(makeTodoParameter()).execute()
                } else {
                    request.updateTodo(id, makeTodoParameter()).execute()
                }
            if (response.isSuccessful) {
                withContext(Main) {
                    onSuccess(id != null)
                }
            } else {
                val body =
                    ApiClient().gson.fromJson(
                        response.errorBody()?.string(),
                        BasicResponse::class.java
                    )
                withContext(Main) {
                    showErrorAlertDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorAlertDialog(getString(R.string.message_error_exception))
            }
        }
    }

    private fun makeTodoParameter(): TodoRequestBody {
        val title = edit_text_title.text.toString()
        val detail = edit_text_detail.text.toString()
        val date = if (text_date_content.text.isBlank()) {
            ""
        } else {
            val parseDate = format.parse(text_date_content.text.toString())
            SimpleDateFormat(FORMAT_DATE_FOR_SERVER, Locale.getDefault()).format(parseDate)
        }

        return TodoRequestBody(title, detail, date)
    }

    private fun onSuccess(isUpdateTodo: Boolean) {
        val receiveMessage =
            if (isUpdateTodo) {
                R.string.message_update_success
            } else {
                R.string.message_registration_success
            }
        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.value =
            Event(getString(receiveMessage))
        findNavController().popBackStack()
    }

    companion object {
        private const val MAX_TITLE_LIMIT = 100
        private const val MAX_DETAIL_LIMIT = 1000
        private const val FORMAT_DATE = "yyyy/M/d"
        private const val FORMAT_DATE_FOR_SERVER = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
}
