package com.example.todo_app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.todo_list_item.view.*

class RecyclerViewAdapter(private val todoItemClickListener: (Todo) -> Unit) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    var todoList: List<Todo> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.todo_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.text_todo_title.text = todoList[position].title
        viewHolder.itemView.setOnClickListener {
            todoItemClickListener.invoke(todoList[position])
        }
    }

    override fun getItemCount() = todoList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
