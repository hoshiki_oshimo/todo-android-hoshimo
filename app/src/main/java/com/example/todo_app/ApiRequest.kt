package com.example.todo_app

import retrofit2.Call
import retrofit2.http.*

interface ApiRequest {
    @GET("todos")
    fun fetchTodos(): Call<TodosGetResponse>

    @POST("todos")
    fun createTodo(@Body TodoRequestBody: TodoRequestBody): Call<BasicResponse>

    @PUT("todos/{id}")
    fun updateTodo(
        @Path("id") id: Int,
        @Body TodoRequestBody: TodoRequestBody
    ): Call<BasicResponse>

    @DELETE("todos/{id}")
    fun deleteTodo(@Path("id") id: Int): Call<BasicResponse>
}
