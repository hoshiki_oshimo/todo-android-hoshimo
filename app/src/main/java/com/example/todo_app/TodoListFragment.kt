package com.example.todo_app

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_todo_list.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TodoListFragment : Fragment() {
    private var isDeleteMode = false
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.observeEvent(this) {
            Snackbar.make(requireView(), it, Snackbar.LENGTH_LONG).show()
        }
        return inflater.inflate(R.layout.fragment_todo_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layout = LinearLayoutManager(activity)
        recycler_view.layoutManager = layout
        adapter = RecyclerViewAdapter(::onClickTodoItem)
        recycler_view.adapter = adapter
        val itemDecoration = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        recycler_view.addItemDecoration(itemDecoration)
        CoroutineScope(Dispatchers.Default).launch(Dispatchers.IO) {
            fetchTodoList()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_todo_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_add_button -> {
                goTodoListFragment()
            }
            R.id.menu_delete_button -> {
                isDeleteMode = !isDeleteMode
                val setIcon =
                    if (isDeleteMode) R.drawable.ic_quit_delete_mode_icon else R.drawable.ic_delete_mode_icon
                item.setIcon(setIcon)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private suspend fun fetchTodoList() {
        try {
            val response = ApiClient().apiRequest.fetchTodos().execute()
            withContext(Main) {
                if (response.isSuccessful) {
                    adapter.todoList = response.body()?.todos!!
                } else {
                    val body =
                        ApiClient().gson.fromJson(
                            response.errorBody()?.string(),
                            BasicResponse::class.java
                        )
                    showErrorAlertDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorAlertDialog(getString(R.string.message_error_exception))
            }
        }
    }

    private suspend fun deleteTodo(id: Int) {
        try {
            val response = ApiClient().apiRequest.deleteTodo(id).execute()
            if (response.isSuccessful) {
                fetchTodoList()
            } else {
                val body =
                    ApiClient().gson.fromJson(
                        response.errorBody()?.string(),
                        BasicResponse::class.java
                    )
                withContext(Main) {
                    showErrorAlertDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorAlertDialog(getString(R.string.message_error_exception))
            }
        }
    }

    private fun onClickTodoItem(todo: Todo) {
        if (isDeleteMode) {
            showDeleteDialog(todo.id, todo.title)
        } else {
            goTodoListFragment(todo)
        }
    }

    private fun goTodoListFragment(todo: Todo? = null) {
        isDeleteMode = false
        val action = TodoListFragmentDirections.actionToTodoEdit(todo)
        findNavController().navigate(action)
    }

    private fun showDeleteDialog(id: Int, title: String) {
        AlertDialog.Builder(requireContext())
            .setMessage(getString(R.string.message_delete_dialog, title))
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                CoroutineScope(Default).launch {
                    deleteTodo(id)
                }
            }
            .show()
    }
}
